import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
import cv2
from PIL import Image, ImageTk

def open_video_file():
    global cap, video_loaded, frame_rate, width, height, canvas
    file_path = filedialog.askopenfilename(
        title="Open Video File",
        filetypes=[("Video Files", "*.mp4;*.avi;*.mkv;*.wmv"), ("All Files", "*.*")]
    )
    if file_path:
        cap = cv2.VideoCapture(file_path)
        if cap.isOpened():
            video_loaded = True
            frame_rate = cap.get(cv2.CAP_PROP_FPS)
            width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
            height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
            canvas.config(width=width, height=height)
            update_video_frame()
        else:
            print("Error opening video file:", file_path)

def record_from_camera():
    global cap, recording, frame_rate, width, height, canvas
    cap = cv2.VideoCapture(0)  # Replace 0 with the desired camera index if multiple cameras are present
    recording = True
    frame_rate = cap.get(cv2.CAP_PROP_FPS)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    canvas.config(width=width, height=height)
    update_video_frame()

def stop_recording():
    global recording
    recording = False

def play_video():
    global paused
    paused = False
    update_video_frame()

def pause_video():
    global paused
    paused = True

def forward_10_seconds():
    global cap, paused
    if paused:
        return
    current_frame = cap.get(cv2.CAP_PROP_POS_FRAMES)
    cap.set(cv2.CAP_PROP_POS_FRAMES, max(0, current_frame + 10 * frame_rate))
    update_video_frame()

def backward_10_seconds():
    global cap, paused
    if paused:
        return
    current_frame = cap.get(cv2.CAP_PROP_POS_FRAMES)
    cap.set(cv2.CAP_PROP_POS_FRAMES, max(0, current_frame - 10 * frame_rate))
    update_video_frame()

def change_speed(speed):
    global cap, paused
    if paused:
        return
    try:
        # Attempt to adjust playback speed directly (experimental)
        cap.set(cv2.CAP_PROP_POS_AVI_RATIO, float(speed))
    except:
        print("Warning: Unable to directly adjust playback speed. Using frame rate adjustment instead.")
        # Fallback to frame rate adjustment if direct speed control fails
        update_video_frame()

def update_video_frame():
    global cap, recording, paused, canvas, video_loaded, frame_rate, width, height
    ret, frame = cap.read()

    if ret:
        if recording:
            # Implement video recording logic here
            try:
                recording_writer.write(frame)  # Assuming a VideoWriter object is initialized
            except:
                print("Error during video recording.")

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        img = ImageTk.PhotoImage(Image.fromarray(frame))
        canvas.create_image(0, 0, anchor=tk.NW, image=img)

        if not paused:
            root.after(int(1000 / frame_rate), update_video_frame)  # Factor in speed for scheduling
    else:
        # Video playback/recording ended
        cap.release()
        stop_recording()
        video_loaded = False

# Create the main window
root = tk.Tk()
root.title("Video Player")

# Configure style for a more modern look
style = ttk.Style()
style.theme_use('clam')  # Choose a theme ('clam', 'alt', 'default', 'classic')

# Create canvas for displaying video frames
canvas = tk.Canvas(root)
canvas.pack(pady=10)

# Create buttons for various controls with improved layout
button_frame = ttk.Frame(root)
button_frame.pack(pady=10)

open_button = ttk.Button(button_frame, text="Open File", command=open_video_file)
open_button.grid(row=0, column=0, padx=5)
record_button = ttk.Button(button_frame, text="Record", command=record_from_camera)
record_button.grid(row=0, column=1, padx=5)
play_button = ttk.Button(button_frame, text="Play", command=play_video)
play_button.grid(row=0, column=2, padx=5)
pause_button = ttk.Button(button_frame, text="Pause", command=pause_video)
pause_button.grid(row=0, column=3, padx=5)
forward_button = ttk.Button(button_frame, text="Forward 10s", command=forward_10_seconds)
forward_button.grid(row=0, column=4, padx=5)
backward_button = ttk.Button(button_frame, text="Backward 10s", command=backward_10_seconds)
backward_button.grid(row=0, column=5, padx=5)

# Speed Scale with labeled frame
speed_frame = ttk.Frame(root)
speed_frame.pack(pady=10)
speed_label = ttk.Label(speed_frame, text="Speed")
speed_label.pack(side=tk.LEFT, padx=5)
speed_scale = ttk.Scale(speed_frame, from_=0.5, to=2.0, orient=tk.HORIZONTAL, length=200, command=change_speed)
speed_scale.pack(side=tk.LEFT, padx=5)

# Initialize global variables
cap = None
video_loaded = False
paused = True
recording = False
frame_rate = None
width = None
height = None
recording_writer = None

# Run the main loop
root.mainloop()
