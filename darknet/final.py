import cv2
import numpy as np
import time
from twilio.rest import Client  # Twilio for SMS notification (Optional)
from plyer import notification  # plyer for desktop notifications

# Twilio Account SID and Auth Token (Replace with yours if using SMS)
account_sid = 'AC2df721ffbd7251031f85cfc501013f61'
auth_token = '70d074ccaaaebe05553437f7e6792cf1'
client = Client(account_sid, auth_token)  # Initialize Twilio client (Optional)

# Function to send alarm notification (SMS or Desktop)
def send_alarm_notification(notification_type="desktop"):
    if notification_type == "sms":  # Use Twilio for SMS (Optional)
        client.messages.create(
            to='your_shopkeeper_phone_number',
            from_='Twilio_Number',
            body='Suspicious activity detected! Someone might be leaving without paying.'
        )
    elif notification_type == "desktop":  # Use plyer for desktop notification
        notification.notify(
            title='Suspicious Activity Detected',
            message='Someone is trying to leave without paying!',
            app_icon=None,  # You can set an icon for the notification
            timeout=10  # Notification will disappear after 10 seconds
        )

# Define object detection model (Choose a suitable model based on your needs)
# Replace with your preferred model's loading code and class names
def load_object_detection_model():
    # Example using YOLOv5 (replace with your model)
    model = cv2.dnn.readNet("cfg/yolov3.weights", "cfg/yolov3.cfg")
    class_names = []
    with open("data/coco.names", "r") as f:
        class_names = [line.strip() for line in f.readlines()]
    return model, class_names

# Function to detect objects in a frame
def detect_objects(frame, model, class_names, confidence_threshold=0.5):
    outputs = []  # Store detection results
    blob = cv2.dnn.blobFromImage(frame, 1/255.0, (416, 416), swapRB=True, crop=False)
    model.setInput(blob)
    layer_names = model.getLayerNames()
    output_layers = [layer_names[i - 1] for i in model.getUnconnectedOutLayers()]
    outs = model.forward(output_layers)

    # Process detections
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > confidence_threshold:
                # Object detected
                box = detection[0:4] * np.array([frame.shape[1], frame.shape[0], frame.shape[1], frame.shape[0]])
                (centerX, centerY, width, height) = box.astype("int")
                x = int(centerX - (width / 2))
                y = int(centerY - (height / 2))

                outputs.append((class_id, confidence, x, y, x + width, y + height))

    return outputs

# Load pre-trained object detection model and class names
model, class_names = load_object_detection_model()

# Define region of interest (ROI) for object detection (optional)
# Adjust these coordinates based on your shop layout
roi_x1, roi_y1, roi_x2, roi_y2 = 100, 100, 500, 400  # Example ROI coordinates

# Capture video from the camera
cap = cv2.VideoCapture(0)  # Change to video file path if desired

# Track consecutive frames with person detection to reduce false alarms
consecutive_person_frames = 0
detection_threshold = 1  # Number of consecutive frames required for alarm

while True:
    ret, frame = cap.read()
    if not ret:
        break

    # Apply region of interest (optional)
    if roi_x1 < roi_x2 and roi_y1 < roi_y2:
        frame = frame[roi_y1:roi_y2, roi_x1:roi_x2]

    # Detect objects in the frame
    # Detect objects in the frame
    outputs = detect_objects(frame, model, class_names)

    # Track person detections
    is_person_detected = False
    for class_id, confidence, x, y, w, h in outputs:
        class_name = class_names[class_id]
        if class_name == "person" and confidence > 0.5:
            is_person_detected = True
            cv2.rectangle(frame, (x, y), (w, h), (0, 255, 0), 2)  # Draw green box around person

    # Handle consecutive person detection and send notification
    if is_person_detected:
        consecutive_person_frames += 1
        if consecutive_person_frames >= detection_threshold:
            send_alarm_notification()
            consecutive_person_frames = 0  # Reset counter after notification
    else:
        consecutive_person_frames = 0  # Reset counter if no person detected

    # Display the resulting frame
    cv2.imshow('Shop Surveillance', frame)

    # Exit on 'q' key press
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release resources
cap.release()
cv2.destroyAllWindows()
